#!/usr/bin/perl
use strict;
use warnings;
use Test::More;
use lib('./lib');

BEGIN {
	use_ok('Nurse::Utils', qw/get_mcpan/);
}

subtest 'get mcpan' => sub {
	my $mcpan = get_mcpan();
	isa_ok($mcpan, 'MetaCPAN::Client');
};

done_testing();


package Nurse::Utils;
use strict;
use warnings;
use v5.24.1;
use CHI;
use WWW::Mechanize::Cached;
use HTTP::Tiny::Mech;
use MetaCPAN::Client;
use File::Temp qw/tempdir/;
use Sub::Exporter::Progressive -setup => { exports => [qw( get_mcpan )], };

sub get_mcpan {
    state $tempdir = tempdir( CLEANUP => 1 );
    state $mcpan = MetaCPAN::Client->new(
        ua => HTTP::Tiny::Mech->new(
            mechua => WWW::Mechanize::Cached->new(
                cache => CHI->new(
                    driver   => 'File',
                    root_dir => $tempdir,
                ),
            ),
        ),
    );

    return $mcpan;
}

1;

FROM perl:5.26.0

ADD . /nurse/

WORKDIR /nurse/

RUN apt -y update && apt -y install libgd-dev

RUN curl -sL --compressed https://git.io/cpm > cpm && chmod +x cpm

RUN ./cpm install -g

RUN perl -c script/pause-packages.pl
RUN perl -c script/generate-graph.pl

#!/usr/bin/env perl

use strict;
use warnings;
use utf8;
use v5.24.1;

use GD::Graph::bars;
use GD::Graph::hbars;
use GD::Graph::Data;
use DBI;

sub main {
    my $dbh = DBI->connect( "dbi:SQLite:dbname=metacpan", "", "" );

    my $captions = [qw/unknown >0<10 >10<20 >20<10 >30<40 >40<50 >50<60 >60<70 >70<80 >80<90 >90<100 100/];
    my $values   = _get_values($dbh);

    my $data = GD::Graph::Data->new( [ $captions, $values, ] )
        or die GD::Graph::Data->error;

    my $my_graph = GD::Graph::bars->new( 800, 400 );
    my $name = 'dist-cover';

    print STDERR "Processing $name\n";

    $my_graph->set(
        x_label => 'cover %',
        y_label => 'distributions',
        title   => 'MetaCPAN code coverage',

        # shadows
        bar_spacing  => 18,
        shadow_depth => 4,
        shadowclr    => 'dred',

        transparent => 0,
    ) or warn $my_graph->error;

    $my_graph->plot($data) or die $my_graph->error;
    save_chart( $my_graph, $name );
}

sub save_chart {
    my ( $chart, $caption ) = @_;

    my $ext = $chart->export_format;

    open( my $out, '>', "./resources/$caption.$ext" )
        or die "Cannot open '$caption.$ext' for write: $!";
    binmode $out;
    print $out $chart->gd->$ext();
    close $out;
}

sub _get_values {
    my ($dbh) = @_;

    my @res;

    my $res = $dbh->selectrow_hashref("SELECT count(*) FROM metacpan WHERE cover = ''");
    push @res, $res->{'count(*)'} if !!$res;

    my $sth  = $dbh->prepare('SELECT count(*) FROM metacpan WHERE cover >= ? and cover < ?');
    my @cond = (
        [ 0,  10 ],
        [ 10, 20 ],
        [ 20, 30 ],
        [ 30, 40 ],
        [ 40, 50 ],
        [ 50, 60 ],
        [ 60, 70 ],
        [ 70, 80 ],
        [ 80, 90 ],
        [ 90, 100 ],
    );
    for my $cond (@cond) {
        $sth->execute(@$cond);
        my $res = $sth->fetch;
        push @res, $res->[0];
    }

    $res = $dbh->selectrow_hashref("SELECT count(*) FROM metacpan WHERE cover = 100");
    push @res, $res->{'count(*)'} if !!$res;

    return \@res;
}

main();

1;

#!/usr/bin/env perl
use strict;
use warnings;
use 5.26.0;
use autodie;
use Carp qw/croak/;
use LWP::Simple qw/getstore/;
use File::Slurp;
use File::Temp qw/tempfile tempdir/;
use File::Copy;
use Archive::Tar;
use Cwd qw/getcwd/;
use lib('./lib');
use Nurse::Utils qw/get_mcpan/;

main( get_release( shift @ARGV ) );

sub main {
    my ($release) = @_;

    my ( undef, $filename ) = tempfile();
    my $tempdir = tempdir( CLEANUP => 1 );

    my $url = $release->download_url;
    getstore( $url, $filename );

    my $archive = Archive::Tar->new();
    $archive->read($filename);
    $archive->setcwd($tempdir);
    $archive->extract();
    my $release_name = $release->distribution;
    my $work_dir     = $tempdir . "/" . $release_name;
    my $cwd          = getcwd();

    rename( $tempdir . "/" . $release->name, $work_dir );    # for minil

    chdir($work_dir);

    my $dockerfile = -e 'Makefile.PL' ? 'Dockerfile_Makefile' : 'Dockerfile_Build';
    system('minil migrate') if $dockerfile eq 'Dockerfile_Build' && !-e 'Build.PL';

    my $dockerfile_template = read_file( $cwd . '/templates/' . $dockerfile );
    write_file( $tempdir . "/Dockerfile", sprintf( $dockerfile_template, $release_name ) ) or croak "Can't write new Dockerfile at [$tempdir]: $!\n";
    add_entrypoint($tempdir, $release_name);

    my $cover_name = $release_name . ".cover";
    chdir($tempdir);
    if ( system( "docker build -t cynovg/" . lc($release_name) . " . >$cwd/logs/$release_name.log 2>$cwd/logs/$release_name.log" ) == 0 ) {
        system( "docker run --rm cynovg/" . lc($release_name) . " >$cover_name 2>>$cwd/logs/$release_name.warn" );
        rename( $cover_name, "$cwd/result/$cover_name" );
    }
    else {
        warn sprintf( "%s\n", $release->name );
    }
}

sub get_release {
    my ($release_name) = @_;

    croak "Set release name first" unless !!$release_name;

    my $mcpan   = get_mcpan();
    my $release = $mcpan->release($release_name);

    croak "Can't find release" unless !!$release;

    return $release;
}

sub add_entrypoint {
    my ( $dir, $release_name ) = @_;

    my $entrypoint = <<ENTRYPOINT;
#!/bin/bash

cd /%s
cover -test

ENTRYPOINT

    write_file( "$dir/entrypoint.sh", sprintf( $entrypoint, $release_name ) );
	chmod(0755, "$dir/entrypoint.sh");
    return;
}

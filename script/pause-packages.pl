#!/usr/bin/env perl 

use strict;
use warnings;
use utf8;
use v5.24.1;
use open qw/:std :utf8/;
use DBI;
use Try::Tiny;
use lib('./lib');
use Nurse::Utils qw/get_mcpan/;

# CREATE TABLE metacpan(name text, cover real, date text, version test, first text, download_url text);

sub main {
    my $dbh = DBI->connect( "dbi:SQLite:dbname=metacpan", "", "" );
    my $inser_sth = $dbh->prepare("INSERT INTO metacpan VALUES(?, ?, ?, ?, ?, ?)");

	my $mcpan = get_mcpan();

    for my $author ( _get_authors() ) {
        my $releases
            = $mcpan->release( { all => [ { author => $author }, { status => 'latest' }, ] }, { scroll_site => 1001 } );
        my $c = 0;
        while ( my $release = $releases->next ) {
            last if $c++ > 5000;    # see https://github.com/metacpan/metacpan-client/issues/99
            if ( $dbh->selectrow_hashref( "SELECT name FROM metacpan WHERE name = ?", undef, $release->distribution ) ) {
                warn sprintf( "release [%s] skip by exist\n", $release->distribution );
                next;
            }

            my $cover = $mcpan->cover( $release->name );

            $inser_sth->execute(
                $release->distribution // '',
                $cover->criteria ? $cover->criteria->{'total'} // '' : '',
                $release->date // '',
                $release->version // '',
                $release->first // '',
                $release->download_url // '',
            );

            printf(
                "%-40s %3s\t%20s %-10s %1s %s\n",
                $release->distribution // '',
                $cover->criteria ? $cover->criteria->{'total'} // '' : '',
                $release->date // '',
                $release->version // '',
                $release->first // '',
                $release->download_url // '',
            );
        }
    }
}

sub _get_authors {
    my $mcpan = get_mcpan();

    my @authors;
    for my $letter ( 'A' .. 'Z' ) {
        my $authors = $mcpan->author( { pauseid => $letter . '*' } );
        my $c = 0;
        while ( my $author = $authors->next ) {
            next if $author->name && $author->name eq 'Deleted';
            push @authors, $author->pauseid;
            last if $c++ > 5000;    # see https://github.com/metacpan/metacpan-client/issues/99
        }
    }

    return @authors;
}



main();

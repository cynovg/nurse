#!/usr/bin/perl
use strict;
use warnings;
use DBI;

my $limit = 10;

my $dbh = DBI->connect( "dbi:SQLite:dbname=metacpan", "", "" );
my $sth = $dbh->prepare("SELECT name FROM metacpan WHERE cover = ''");
$sth->execute();
my $c = 0;
while (my $release = $sth->fetch) {
	system("script/check_cover_release.pl $release->[0]");
	last if $c++ >= $limit;
}